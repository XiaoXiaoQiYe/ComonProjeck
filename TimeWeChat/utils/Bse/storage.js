module.exports = {
  put: put,
  get: get,
  remove: remove,
  clear: clear,
  tables:table,
}
/*设置缓存：  util.put('key','value',20) 表示设置缓存失效时间为20秒；

获取缓存：util.get('key')  

清除缓存：util.remove('key')

清除所有缓存：util.clear()
*/
function put(k, v) {
  wx.setStorageSync(k, v);
}
 
function get(k, def) {
  var res = wx.getStorageSync(k);
  if (res) {
    return res;
  } else {
    return def;
  }
}
 
function remove(k) {
  wx.removeStorageSync(k);
}
 
function clear() {
  wx.clearStorageSync();
}
var table = {
  // 当前专注任务
  absorbed:{
    key:"absorbed",
    val:{
      // 标签
      target:"",
      // 分钟数
      minuteNumber:"",
      // 创建时间
      createDateTime:"",
      // 状态 0-暂停，1-执行中，9-结束
      state:"",
    },
    
  },
  // 正在执行的
  currentAbsorbed:{
    key:"currentAbsorbed",
    // 执行的数据
    val:""
  }
}